import {createApp, h, provide} from 'vue'
import App from './App.vue'
import { router } from "./router";
import { DefaultApolloClient } from "@vue/apollo-composable";
import apolloClient from "./graphql";
import './index.css'
import Keycloak from 'keycloak-js'
const keycloak = new Keycloak('/keycloak.json');
import Topbar from './components/Topbar.vue'
let initOptions = {
  url: 'https://auth.shockbyte.dev/auth', realm: 'shockbyte', clientId: 'videe-frontend',
}

keycloak.init({ onLoad: initOptions.onLoad }).then((auth) => {
  createApp({
    setup () {
      provide('keycloak', keycloak)
      provide(DefaultApolloClient, apolloClient)
    },
    render: () => h(App),
  }).use(router).component('Topbar', Topbar).mount('#app')
}).catch(() => {
  console.log('Auth failed');
});


