import {createRouter, createWebHistory} from 'vue-router';
import Index from './pages/Index.vue'
import Room from './pages/Room.vue'
import Random from './pages/Random.vue'
import NotFound from './pages/NotFound.vue'

const routes = [
  { path: '/', component: Index, name: 'index' },
  { path: '/random/:id', component: Random, name: 'random' },
  { path: '/room/:id', component: Room, name: 'room'},
  { path: '/:pathMatch(.*)', component: NotFound, name: 'notFound' },
]


export const router = createRouter({
  history: createWebHistory(),
  routes,
})
