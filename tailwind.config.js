module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'main-bg': '#4F00B4',
        'separator-1': '#340076',
        'input-background': '#430099',
        'input-placeholder': '#7000FF',
        'button-background': '#D2E400',
        'button-text': '#8D9900',
        'landing-text': '#A966FF',
        'lading-highlight': '#C699FF',
        'room-list-bg': '#4F00B4',
        'room-header-bg': '#5A00CC',
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
